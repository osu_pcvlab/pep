#include "markerdesign.h"
// scale
#define radius 0.018 // 20 * 5
#define radius2 20 * 5

// Makrer settings
#define offset 6*radius
#define offset_x 6*radius2
#define offset_y 6*radius2
#define origin cv::Point(offset_x,offset_y)
#define widthMarker 12*radius2+1
#define heightMarker 12*radius2+1
// Marker code setting
#define angle_inc 30 // it devides a circle (360 degrees) to segments of 30 degrees
#define min_radius 0.25*radius // inner radius that code does not violate inside it
#define max_radius 1*radius // out radius for code
// Ground truth setting
#define center_x 21.25*radius
#define center_y 21.25*radius
#define Origin_GT cv::Point(center_x,center_y)
#define extent_x 42.5*radius+1
#define extent_y 42.5*radius+1
#define gt_isOff true
// This function creates an image that has the marker in the middle
// and 8 lines (segments).
int code[360/angle_inc]={1,1,1,0,0,0,1,0,0,1,1,0};

MarkerDesign::MarkerDesign()
{
}

std::vector<cv::RotatedRect> MarkerDesign::Design(cv::Point2f &center)
{
    // create marker 8 inch by 8 inch in the marker space
    // Output: a vector of 8 rotated rectangles (circles)
    // Output (by reference): center of the marker
    cv::RotatedRect m1;
    m1.center.x = -std::sqrt(2) * 3 * radius + offset;
    m1.center.y = 0 + offset;
    m1.size.height = 2 * radius;
    m1.size.width = 2 * radius;
    m1.angle = 0.;

    cv::RotatedRect m2;
    m2.center.x = -3 * radius + offset;
    m2.center.y = - 3 * radius + offset;
    m2.size.height = 2 * radius;
    m2.size.width = 2 * radius;
    m2.angle = 0.;

    cv::RotatedRect m3;
    m3.center.x = 0 + offset;
    m3.center.y = -std::sqrt(2) * 3 * radius + offset;
    m3.size.height = 2 * radius;
    m3.size.width = 2 * radius;
    m3.angle = 0;

    cv::RotatedRect m4;
    m4.center.x =  3 * radius + offset;
    m4.center.y = -3 * radius + offset;
    m4.size.height = 2 * radius;
    m4.size.width = 2 * radius;
    m4.angle = 0;

    cv::RotatedRect m5;
    m5.center.x = std::sqrt(2) * 3 * radius + offset;
    m5.center.y = 0 + offset;
    m5.size.height = 2 * radius;
    m5.size.width = 2 * radius;
    m5.angle = 0;

    cv::RotatedRect m6;
    m6.center.x = 3 * radius + offset;
    m6.center.y = 3 * radius + offset;
    m6.size.height = 2 * radius;
    m6.size.width = 2 * radius;
    m6.angle = 0;

    cv::RotatedRect m7;
    m7.center.x = 0 + offset;
    m7.center.y = std::sqrt(2) * 3 * radius + offset;
    m7.size.height = 2 * radius;
    m7.size.width = 2 * radius;
    m7.angle = 0;

    cv::RotatedRect m8;
    m8.center.x = -3 * radius + offset;
    m8.center.y = 3 * radius + offset;
    m8.size.height = 2 * radius;
    m8.size.width = 2 * radius;
    m8.angle = 0;

    std::vector<cv::RotatedRect> marks;
    marks.push_back(m1);
    marks.push_back(m2);
    marks.push_back(m3);
    marks.push_back(m4);
    marks.push_back(m5);
    marks.push_back(m6);
    marks.push_back(m7);
    marks.push_back(m8);

    center.x = offset;
    center.y = offset;

    return marks;
}

cv::Mat MarkerDesign::Visualize_Marker(std::vector<cv::RotatedRect> marks)
{
    // create marker in the image space
    // input: the marker in the marker space, a vector of 8 roated rectngles.
    // Output: the marker in the image space. This image can be plotted and installed on the walls as markers.
    cv::Mat marker(widthMarker,heightMarker,CV_8UC3,cv::Scalar(255,255,255));
    for (int i = 0; i < marks.size() ; i++)
    {
        cv::RotatedRect mark = marks[i];
        cv::RotatedRect m;
        m.center = mark.center * (radius2 / radius);
        m.size.height = mark.size.height * (radius2 / radius);
        m.size.width = mark.size.width * (radius2 / radius);
        m.angle = mark.angle;
        cv::ellipse(marker,m,cv::Scalar(0,0,255),cv::FILLED);
    }
    cv::circle(marker,cv::Point(0 ,0 ) + origin, 0 , cv::Scalar(0,0,0), 0,cv::FILLED );
    std::vector<int> params;
    params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    params.push_back(10);
    cv::imwrite("marker.png",marker,params);
    marker.release();
    cv::Mat marker2(widthMarker,heightMarker,CV_8UC3,cv::Scalar(255,255,255));
    return marker2;
}

void MarkerDesign::Visualize(cv::Mat canvas , std::vector<cv::RotatedRect> marks ,
                             std::vector<cv::RotatedRect> marks2 , std::string name)
{
    cv::Mat canvas2 = canvas.clone();
    for (int i = 0 ; i < marks.size() ; i++)
    {
        double min_dist = INFINITY;
        int min_element = -1;
        for (int j = 0 ; j < marks2.size() ; j++)
        {
            double dist = cv::norm(marks[i].center - marks2[j].center);
            if (min_dist > dist)
            {
                min_dist = dist;
                min_element = j;
            }
        }
        cv::RotatedRect mark = marks[i];
        cv::RotatedRect mark2 = marks2[min_element];

        cv::Point2f p1 = mark.center * (radius2 / radius);
        cv::Point2f p2 = mark2.center * (radius2 / radius);

        cv::RotatedRect m1;
        m1.center = p1;
        m1.size.height = mark.size.height * (radius2 / radius);
        m1.size.width = mark.size.width * (radius2 / radius);
        m1.angle = mark.angle;

        cv::RotatedRect m2;
        m2.center = p2;
        m2.size.height = mark2.size.height * (radius2 / radius);
        m2.size.width = mark2.size.width * (radius2 / radius);
        m2.angle = mark2.angle;

        //std::cout << " p1 = " << p1 << " p2 = " << p2 << std::endl;
        cv::ellipse(canvas2,m2,cv::Scalar(255,0,0),1);
        cv::ellipse(canvas2,m1,cv::Scalar(0,0,255),1);

        cv::arrowedLine(canvas2 , p1 , p2 , cv::Scalar(0 , 0, 255) , 2 , 8 , 0 ,1);
    }
    std::vector<int> params;
    params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    params.push_back(9);
    cv::imwrite(name,canvas2,params);
    canvas2.release();

    return;
}

double MarkerDesign::getRadius()
{
    return radius;
}

void MarkerDesign::Visualize_Back_Fore_Proj(cv::Mat canvas , std::vector<cv::RotatedRect> marks ,
                             std::vector<cv::RotatedRect> marks_ , std::string name)
{
    // This function plots the marker in the image space and the back-projected
    // marker in the marker space into the image space.
    // input: the marker in the image space (canvas),
    // input: the marker in the marker space (marks),
    // input: the marker in the
    cv::Mat canvas2 = canvas.clone();
    for (int i = 0 ; i < marks.size() /2 ; i++)
    {
        double min_dist1 = INFINITY;
        int min_element1 = -1;
        for (int j = 0 ; j < marks_.size() ; j++)
        {
            double dist = cv::norm(marks[i].center - marks_[j].center);
            if (min_dist1 > dist)
            {
                min_dist1 = dist;
                min_element1 = j;
            }
        }

        double min_dist2 = INFINITY;
        int min_element2 = -1;
        for (int j = 0 ; j < marks_.size() ; j++)
        {
            double dist = cv::norm(marks[i+4].center - marks_[j].center);
            if (min_dist2 > dist)
            {
                min_dist2 = dist;
                min_element2 = j;
            }
        }

        cv::RotatedRect mark1 = marks[i];
        cv::RotatedRect mark2 = marks[i+4];
        cv::RotatedRect mark_1 = marks_[min_element1];
        cv::RotatedRect mark_2 = marks_[min_element2];

        cv::Point2f p1 = mark1.center * (radius2 / radius);
        cv::Point2f p2 = mark2.center * (radius2 / radius);
        cv::Point2f p_1 = mark_1.center * (radius2 / radius);
        cv::Point2f p_2 = mark_2.center * (radius2 / radius);

        cv::RotatedRect m1;
        m1.center = p1;
        m1.size.height = mark1.size.height * (radius2 / radius);
        m1.size.width = mark1.size.width * (radius2 / radius);
        m1.angle = mark1.angle;

        cv::RotatedRect m2;
        m2.center = p2;
        m2.size.height = mark2.size.height * (radius2 / radius);
        m2.size.width = mark2.size.width * (radius2 / radius);
        m2.angle = mark2.angle;

        cv::RotatedRect m_1;
        m_1.center = p_1;
        m_1.size.height = mark_1.size.height * (radius2 / radius);
        m_1.size.width = mark_1.size.width * (radius2 / radius);
        m_1.angle = mark_1.angle;

        cv::RotatedRect m_2;
        m_2.center = p_2;
        m_2.size.height = mark_2.size.height * (radius2 / radius);
        m_2.size.width = mark_2.size.width * (radius2 / radius);
        m_2.angle = mark_2.angle;

        //std::cout << " p1 = " << p1 << " p2 = " << p2 << std::endl;
        cv::ellipse(canvas2,m_1,cv::Scalar(255,0,0),1);
        cv::ellipse(canvas2,m_2,cv::Scalar(255,0,0),1);


        cv::ellipse(canvas2,m1,cv::Scalar(0,0,255),1);
        cv::ellipse(canvas2,m2,cv::Scalar(0,0,255),1);

        cv::line(canvas2,p1,p2,cv::Scalar(0,0,255),2);
        cv::line(canvas2,p_1,p_2,cv::Scalar(255,0,0),1);

        cv::arrowedLine(canvas2 , p1 , p_1 , cv::Scalar(0 , 0, 255) , 2 , 8 , 0 ,1);
        cv::arrowedLine(canvas2 , p2 , p_2 , cv::Scalar(0 , 0, 255) , 2 , 8 , 0 ,1);

    }
    std::vector<int> params;
    params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    params.push_back(9);
    cv::imwrite(name,canvas2,params);
    canvas2.release();

    return;
}
