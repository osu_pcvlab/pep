#ifndef MARKEREVALUATION_H
#define MARKEREVALUATION_H
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/optim/optim.hpp>

class MarkerEvaluation
{
public:
    MarkerEvaluation();
    std::vector<double> ErrorEvaluation
        (std::vector<cv::RotatedRect> markers,
         std::vector<cv::RotatedRect> projectedMarker);
    cv::Point2f CalcCenterBasedOnHomography(cv::Point2f c, cv::Mat H);
    cv::Point2f CalcCenterBasedOnEllipses
        (std::vector<cv::RotatedRect> obsMarker , double &var);
    std::vector<cv::RotatedRect> Projection
        (std::vector<cv::RotatedRect> marker , cv::Mat H);
};

#endif // MARKEREVALUATION_H
