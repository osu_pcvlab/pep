#include "markerevaluation.h"

MarkerEvaluation::MarkerEvaluation()
{
}

static cv::Mat Ellipse2Conic(cv::RotatedRect ellipse)
{
    // Ellipse2Conic: This function converts the ellipse from rotated rectangle representation
    // to the matrix representation.
    // Input: a rotated rectangle,
    // Output: a 3 by 3 matrix.
    double a = 0.5 * ellipse.size.width;
    double b = 0.5 * ellipse.size.height;
    if (a==0 || b==0)
        std::cout << "This ellipse is not right." << std::endl;
    double alfa = 0;//ellipse.angle * M_PI / 180.;
    double x0 = ellipse.center.x;
    double y0 = ellipse.center.y;
    double A = (cos(alfa) * cos(alfa))/(a*a)+(sin(alfa) * sin(alfa)) / (b*b);
    double B = -2 * cos(alfa) * sin(alfa) * (1 / (a*a) - 1 / (b*b) );
    double C = (sin(alfa) * sin(alfa))/(a*a)+(cos(alfa) * cos(alfa)) / (b*b);
    double D = -1 * (2 * A * x0 + y0 * B);
    double E = -1 * (2 * C * y0 + B * x0);
    double F = A * x0 * x0 + C * y0 * y0 + B * x0 * y0 - 1;
    cv::Mat conic = (cv::Mat_<double>(3, 3) <<
                     A , B/2 , D/2,
                     B/2 , C , E/2,
                     D/2 , E/2 , F);
    return conic;
}

static cv::RotatedRect Conic2Ellipse(cv::Mat Conic)
{
    // Conic2Ellipse: This function converts the ellipse from the matrix representation.
    // to the rotated rectangle representation
    // input: a 3 by 3 matrix
    // output: a rotated rectangle.
    double A = Conic.at<double>(0,0);
    double B = Conic.at<double>(0,1);
    double C = Conic.at<double>(1,1);
    double D = Conic.at<double>(0,2);
    double E = Conic.at<double>(1,2);
    double F = Conic.at<double>(2,2);

    cv::Mat C22 = (cv::Mat_<double> (2,2) <<
                   A , B ,
                   B , C );

    double x0 = (C * D - B * E) / (B * B - A * C);
    double y0 = (A * E - B * D) / (B * B - A * C);

    double nom = 2 * ( A * E * E + C * D * D + F * B * B - 2 * B * D * E - A * C * F);
    double part1 = std::sqrt((A - C) * (A - C) + 4 * B * B);

    double denom1 = (B * B - A * C) * (part1 - (A + C));
    double denom2 = (B * B - A * C) * (-part1 - (A + C));

    cv::RotatedRect ellipse;
    ellipse.center.x = x0;
    ellipse.center.y = y0;

    ellipse.size.height = 2 * std::sqrt( nom / denom1 );
    ellipse.size.width = 2 * std::sqrt ( nom / denom2 );
    ellipse.angle = 0;//atan(b / (a - c) )/2;

    return ellipse;
}

/* OrderEllipses: This function sorts the ellipses clockwise. This function
 * prevents conic mismatches later on.
 * Input: The (unorganized) observed ellipses,
 * Output: The ordered observed ellipses.
 */
static std::vector<cv::RotatedRect> OrderEllipses
(std::vector<cv::RotatedRect> unorderedEllipses)
{
    std::vector<cv::RotatedRect> orderedEllipses;
    double x_origin = 0, y_origin = 0;
    int n = unorderedEllipses.size();
    double coeff = 1. / (double)(n);
    for (int i = 0 ; i < n ; i++)
    {
        x_origin = x_origin + coeff * unorderedEllipses[i].center.x;
        y_origin = y_origin + coeff * unorderedEllipses[i].center.y;
    }
    // finding azimuth zero
    double min_dxy = INFINITY;
    double angle_ref;
    for (int i = 0 ; i < n ; i++)
    {
        double dx = unorderedEllipses[i].center.x - x_origin;
        double dy = unorderedEllipses[i].center.y - y_origin;
        if (min_dxy > dx + dy)
        {
            min_dxy = dx + dy;
            angle_ref  = atan2(dy , dx );
        }
    }
    // calculating the angles of ellipses
    std::vector<double> angle_vector;
    for (int i = 0 ; i < n ; i++)
    {
        double dx = unorderedEllipses[i].center.x - x_origin;
        double dy = unorderedEllipses[i].center.y - y_origin;
        double angle = atan2(dy , dx ) - angle_ref;
        if (angle < 0)
            angle = angle + 2 * M_PI;
        if (angle > 2 * M_PI)
            angle = angle - 2 * M_PI;
        angle_vector.push_back(angle);
    }
    // order the ellipses
    std::vector<double> sorted_angle = angle_vector;
    std::sort( sorted_angle.begin(),sorted_angle.end() );
    for (int i = 0 ; i < sorted_angle.size() ; i++)
        for (int j = 0 ; j < angle_vector.size() ; j++)
            if (angle_vector[j] == sorted_angle[i])
            {
                orderedEllipses.push_back(unorderedEllipses[j]);
                continue;
            }
    return orderedEllipses;
}

std::vector<double> MarkerEvaluation::ErrorEvaluation
    (std::vector<cv::RotatedRect> markers, std::vector<cv::RotatedRect> projMarker)
{
    // This function calculates the error between the markers
    // and the back-projected markers from image space
    // input: the markers in the marker space
    //        the back-projected markers from the image space
    // output: the vector error of each marker
    std::vector<double> markersError;
    for (int i = 0 ; i < markers.size() ; i++)
    {
        double min_dist = INFINITY;
        int min_element = -1;
        for (int j = 0 ; j < projMarker.size() ; j++)
        {
            double dist = cv::norm(markers[i].center - projMarker[j].center);
            if (min_dist > dist)
            {
                min_dist = dist;
                min_element = j;
            }
        }
        cv::Point2f p = markers[i].center - projMarker[min_element].center;
        markersError.push_back(cv::norm(p));
    }
    return markersError;
}

cv::Point2f MarkerEvaluation::CalcCenterBasedOnHomography(cv::Point2f c, cv::Mat H)
{
    // This function calculates the center of marker in the image space
    // by projecting the center of marker in the marker space into the image space
    // input: the center of marker in the marker space
    //        the homography transformation
    // output: the center of marker in the image space
    cv::Mat P = (cv::Mat_<double>(3, 1) <<
                     c.x,c.y,1);
    cv::Mat pMat = H * P;
    cv::Point2f p;
    p.x = pMat.at<double>(0,0) / pMat.at<double>(2,0);
    p.y = pMat.at<double>(1,0) / pMat.at<double>(2,0);
    return p;
}
cv::Point2f MarkerEvaluation::CalcCenterBasedOnEllipses
    (std::vector<cv::RotatedRect> obsMarker, double &var)
{
    // This function calculates the center of marker in the image space
    // by intersecting the center of marks of the marker
    // input: the deteccted marks of the marker
    // output: the center of marker in the image space
    //         the variance of the intersected points in pixels
    std::vector<cv::RotatedRect> orderedobsMarker =
            OrderEllipses(obsMarker);
        cv::Mat x0 = (cv::Mat_<double>(3, 1) <<
                    orderedobsMarker[0].center.x, orderedobsMarker[0].center.y , 1);
        cv::Mat x4 = (cv::Mat_<double>(3, 1) <<
                    orderedobsMarker[4].center.x, orderedobsMarker[4].center.y , 1);
        cv::Mat x04 = x0.cross(x4);

        cv::Mat x1 = (cv::Mat_<double>(3, 1) <<
                    orderedobsMarker[1].center.x, orderedobsMarker[1].center.y , 1);
        cv::Mat x5 = (cv::Mat_<double>(3, 1) <<
                    orderedobsMarker[5].center.x, orderedobsMarker[5].center.y , 1);
        cv::Mat x15 = x1.cross(x5);

        cv::Mat x2 = (cv::Mat_<double>(3, 1) <<
                    orderedobsMarker[2].center.x, orderedobsMarker[2].center.y , 1);
        cv::Mat x6 = (cv::Mat_<double>(3, 1) <<
                    orderedobsMarker[6].center.x, orderedobsMarker[6].center.y , 1);
        cv::Mat x26 = x2.cross(x6);

        cv::Mat x3 = (cv::Mat_<double>(3, 1) <<
                    orderedobsMarker[3].center.x, orderedobsMarker[3].center.y , 1);
        cv::Mat x7 = (cv::Mat_<double>(3, 1) <<
                    orderedobsMarker[7].center.x, orderedobsMarker[7].center.y , 1);
        cv::Mat x37 = x3.cross(x7);

        cv::Mat x0415 = x04.cross(x15);
        cv::Mat x0426 = x04.cross(x26);
        cv::Mat x0437 = x04.cross(x37);
        cv::Mat x1526 = x15.cross(x26);
        cv::Mat x1537 = x15.cross(x37);
        cv::Mat x2637 = x26.cross(x37);

        x0415 = x0415 / x0415.at<double>(2,0);
        x0426 = x0426 / x0426.at<double>(2,0);
        x0437 = x0437 / x0437.at<double>(2,0);
        x1526 = x1526 / x1526.at<double>(2,0);
        x1537 = x1537 / x1537.at<double>(2,0);
        x2637 = x2637 / x2637.at<double>(2,0);

        cv::Mat center = (1.0 / 6.0) * (x0415 + x0426 + x0437 + x1526 + x1537 + x2637);

        cv::Mat varMat =
                (x0415 - center).t() * (x0415 - center)  +
                (x0426 - center).t() * (x0426 - center)  +
                (x0437 - center).t() * (x0437 - center)  +
                (x1526 - center).t() * (x1526 - center)  +
                (x1537 - center).t() * (x1537 - center)  +
                (x2637 - center).t() * (x2637 - center)  ;

        center = center / center.at<double>(2,0);
        cv::Point2f centerMarker;
        centerMarker.x = center.at<double>(0,0);
        centerMarker.y = center.at<double>(1,0);
        var = varMat.at<double>(0,0);
        double stand = cv::sqrt(var);

//        std::cout << " center based on detected ellipse = " << centerMarker << std::endl;
//        std::cout << " variance = " << var << std::endl;
//        std::cout << " standard deviation = " << stand << std::endl;
    return centerMarker;
}

/* Projection: projects the ellipses using estimated homography transformation
 * Input: a vector of ellipses in marker/image space,
 * Input: Homography/inverse homorgraphy transformation,
 * Output: a vector of ellipses in image/marker space.
 */
std::vector<cv::RotatedRect> MarkerEvaluation::Projection
    (std::vector<cv::RotatedRect> marker , cv::Mat H)
{
    std::vector<cv::RotatedRect> backProjectedMarker;
    std::cout << "***********************" << std::endl;
    for (int i = 0; i < marker.size(); i++)
    {
        cv::RotatedRect mark = marker[i];
        cv::Mat markMat = Ellipse2Conic(mark);
        cv::Mat backProjectedMark = H.t().inv() * markMat * H.inv();
        cv::RotatedRect backProjectedEll = Conic2Ellipse(backProjectedMark);
        cv::Mat x = (cv::Mat_<double>(3, 1) <<
                         mark.center.x, mark.center.y , 1);
        cv::Mat Hx = H * x;
        Hx = Hx / Hx.at<double>(2,0);
        backProjectedMarker.push_back(backProjectedEll);
    }
    return backProjectedMarker;
}
