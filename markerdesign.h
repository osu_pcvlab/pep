#ifndef MARKERDESIGN_H
#define MARKERDESIGN_H
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
class MarkerDesign
{
public:
    MarkerDesign();
    std::vector<cv::RotatedRect> Design(cv::Point2f &center);
    cv::Mat Visualize_Marker(std::vector<cv::RotatedRect> marks);
    void Visualize(cv::Mat canvas , std::vector<cv::RotatedRect> marks ,
         std::vector<cv::RotatedRect> marks2 ,std::string name);
    void Visualize_Back_Fore_Proj(cv::Mat canvas , std::vector<cv::RotatedRect> marks ,
                    std::vector<cv::RotatedRect> marks_ , std::string name);
    double getRadius();
};

#endif // MARKERDESIGN_H
