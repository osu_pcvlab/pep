#include <QCoreApplication>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/videoio.hpp>
#include <ctime>
#include "markerdetection.h"
#include "markerdesign.h"
#include "homographyestimation.h"
#include "markerevaluation.h"
#define f 12500
#define p_x 800
#define p_y 600

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    cv::Mat temp_img;
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Ideal marker
    cv::Point2f center;
    MarkerDesign* designer = new MarkerDesign();
    std::vector<cv::RotatedRect> markerIdeal = designer->Design(center);
    cv::Mat canvas = designer->Visualize_Marker(markerIdeal);
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // input frame
    cv::Mat input_frame =
        cv::imread("/home/sia/tilt0dist20_7.bmp",cv::IMREAD_ANYCOLOR);
    std::vector<int> params;
    params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    params.push_back(9);
    cv::imwrite("1_original.png",input_frame,params);
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Mark Detection
    MarkerDetection* markerDetector = new MarkerDetection();
    cv::Mat masked_image = markerDetector->ColorMask(input_frame);
    cv::imwrite("2_masked.png",masked_image,params);
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Filtering
    cv::Mat filter_image = markerDetector->Filter(masked_image);
    cv::imwrite("3_filtered.png",filter_image,params);
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Ellipse Fitting
    std::vector<double> error_Marks;
    std::vector<cv::RotatedRect> obsMarks = markerDetector->EillipseFitting
            (filter_image , error_Marks);
    temp_img = input_frame.clone();
    for (int i = 0 ; i < obsMarks.size(); i++)
        cv::ellipse(temp_img , obsMarks[i] ,cv::Scalar(0,0,255),cv::FILLED);
    cv::imwrite("4_ellipse_fitting.png",temp_img,params);
    temp_img.release();
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Mark Pose Estiamtion
    double radius = designer->getRadius();
    cv::Mat K = (cv::Mat_<double>(3, 3) <<
                     f, 0 , p_x,
                     0 , f , p_y,
                     0 , 0 , 1);
    temp_img = input_frame.clone();
    std::vector<cv::Mat> poses = markerDetector->EstimatePose(obsMarks , K , radius);
    std::cout << " ~~~~Marks in Figure 5 :~~~~ " << std::endl;
    for (int i = 0 ; i < poses.size() ; i++)
    {
        cv::RotatedRect r = obsMarks[i];
        cv::ellipse(temp_img , r ,cv::Scalar(0,0,255),cv::FILLED);
        std::string str = boost::lexical_cast<std::string>(i);
        cv::putText(temp_img, str , r.center , 1 , 1.0 ,cv::Scalar(0,0,0),2);
        cv::putText(temp_img, str , r.center , 1 , 1.0 ,cv::Scalar(255,255,255),1);
        std::cout << i << ": poses = " << poses[i] << std::endl;
    }
    cv::imwrite("5_Poses.png",temp_img,params);
    temp_img.release();
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Marker Detection
    std::vector<cv::Mat> Ts;
    std::vector<cv::RotatedRect> obsMarker =
            markerDetector->MarkerFinder(obsMarks , K,radius, Ts);
    temp_img = input_frame.clone();
    for (int i = 0 ; i < obsMarker.size(); i++)
        cv::ellipse(temp_img , obsMarker[i] ,cv::Scalar(0,0,255),cv::FILLED);
    cv::imwrite("41_marker_detection.png",temp_img,params);
    temp_img.release();
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Plotting detected marker
    temp_img = input_frame.clone();
    cv::Point2f origin;
    for (int i = 0 ; i < obsMarker.size(); i++)
        origin = origin + (1. / static_cast<double>(obsMarker.size())) * obsMarker[i].center;
    std::vector<double> xs , ys;
    std::cout << " ~~~~Pose of the marker based on each mark~~~~ " << std::endl;
    for (int i = 0 ; i < obsMarker.size(); i++)
    {
        std::cout << i << " = " << Ts[i] << std::endl;
        cv::ellipse(temp_img , obsMarker[i] ,cv::Scalar(0,0,255),cv::FILLED);
        xs.push_back(obsMarker[i].center.x);
        ys.push_back(obsMarker[i].center.y);
    }
    double h = obsMarker[0].size.height ;
    double w = obsMarker[0].size.width ;
    std::sort(xs.begin() , xs.end());
    std::sort(ys.begin() , ys.end());
    cv::Rect crop(xs[0]- h/ 2.,ys[0]- w/ 2.,xs[7]-xs[0]+h,ys[7]-ys[0]+w);
    cv::Mat temp = temp_img(crop);
    cv::imwrite("6_marker.png",temp,params);
    temp.release();
    temp_img.release();
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Homography Transformation
    HomographyEstimation* homographyEstimator = new HomographyEstimation();
    cv::Mat H = homographyEstimator->HomographyFinder(obsMarker , markerIdeal);
    std::cout << " ~~~~Homography Estimation based on the marks~~~~ " << std::endl;
    std::cout << " H = " << H << std::endl;
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Evaluation
    MarkerEvaluation* evaluator = new MarkerEvaluation();
    cv::Point2f p1 = evaluator->CalcCenterBasedOnHomography(center, H);
    std::cout << " ~~~~Center of the marker in the image based on homography~~~~ " << std::endl;
    std::cout << " p1 = " << p1 << std::endl;
    temp_img = input_frame.clone();
    cv::circle(temp_img , p1 , 10 , cv::Scalar(0,0,255),2);
    cv::circle(temp_img , p1 , 5 , cv::Scalar(0,0,255),1);
    cv::circle(temp_img , p1 , 0 , cv::Scalar(0,0,255),0);
    //
    double variance;
    cv::Point2f p2 = evaluator->CalcCenterBasedOnEllipses(obsMarker,variance);
    std::cout << " ~~~~Center of the marker in the image based on intesection of ellipses' centers~~~~ " << std::endl;
    std::cout << " p2 = " << p2 << std::endl;
    std::cout << " standard deviation = " << std::sqrt(variance) << " pixels"  << std::endl;
    cv::circle(temp_img , p2 , 10 , cv::Scalar(255,0,255),2);
    cv::circle(temp_img , p2 , 5 , cv::Scalar(255,0,255),1);
    cv::circle(temp_img , p2 , 0 , cv::Scalar(255,0,255),0);
    cv::Mat temp_final = temp_img(crop);
    cv::imwrite("7_centered.png",temp_final,params);
    temp_final.release();
    temp_img.release();
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Back-Projection
    std::vector<cv::RotatedRect> bPMarker =
            evaluator->Projection(markerIdeal,H);
    temp_img = input_frame.clone();
    for (int i = 0 ; i < bPMarker.size(); i++)
        cv::ellipse(temp_img , bPMarker[i] ,cv::Scalar(0,0,255),cv::FILLED);
    cv::Mat cropped_temp_img = temp_img(crop);
    cv::imwrite("8_backProjected_marker.png",cropped_temp_img,params);
    cropped_temp_img.release();
    temp_img.release();
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Fore-Projection
    std::vector<cv::RotatedRect> fPMarker =
            evaluator->Projection(obsMarker,H.inv());
    std::string name1 = "9_foreProjected_marker.png";
    cv::Mat canvas2 = canvas.clone();
    cv::Mat canvas3 = canvas.clone();
    //designer->Visualize(canvas2 , markerIdeal , fPMarker , name1);
    designer->Visualize_Back_Fore_Proj(canvas3 , markerIdeal , fPMarker , name1);
    canvas3.release();
    canvas2.release();
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Fore-Projection Error

    std::vector<double> markerSpaceError =
    evaluator->ErrorEvaluation(markerIdeal , fPMarker);
    std::cout << " ~~~~marker_space_error for each mark~~~~ " << std::endl;
    for (int i = 0 ; i < markerSpaceError.size(); i++)
        std::cout << i << " = " << markerSpaceError[i] << " ; ";
    std::cout << std::endl;
    std::cout << " standard deviation = " <<
                 1 / std::sqrt(8) * cv::norm(markerSpaceError) << " meters" << std::endl;
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Back-Projection Error
    std::vector<double> imageSpaceError =
    evaluator->ErrorEvaluation(obsMarker , bPMarker);
    std::cout << " ~~~~image_space_error for each mark~~~~ " << std::endl;
    for (int i = 0 ; i < imageSpaceError.size(); i++)
        std::cout << i << " = " << imageSpaceError[i] << " ; ";
    std::cout << std::endl;
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Back-Projection Error
    cv::Mat centerMarker = (cv::Mat_<double>(3, 1) << p2.x,p2.y,1);
    cv::Mat centerMareker_inMarker = H.inv() * centerMarker;
    cv::Point2f markerC;
    markerC.x = centerMareker_inMarker.at<double>(0,0) / centerMareker_inMarker.at<double>(2,0);
    markerC.y = centerMareker_inMarker.at<double>(1,0) / centerMareker_inMarker.at<double>(2,0);
    //std::cout << " diff = " <<  center - markerC << " meter" << std::endl;
    std::cout << " bias = " << cv::norm(center- markerC) << " meter" << std::endl;
    return a.exec();
}
