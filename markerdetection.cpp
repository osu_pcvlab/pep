#include "markerdetection.h"
#define lower_threshold 10
#define upper_threshold 170
#define out_of_ellipse_thr 10 // pixel
#define max_of_ellipses 20
//#define out_of_homography_thr 1000 // 10 pixels
#define n_marks 8


MarkerDetection::MarkerDetection()
{
}

cv::Mat Ellipse2Conic(cv::RotatedRect ellipse)
{
    // Ellipse2Conic: This function converts the ellipse from rotated rectangle representation
    // to the matrix representation.
    // Input: a rotated rectangle,
    // Output: a 3 by 3 matrix.
    double a = 0.5 * ellipse.size.width;
    double b = 0.5 * ellipse.size.height;
    if (a==0 || b==0)
        std::cout << "This ellipse is not right." << std::endl;
    double alfa = 0;//ellipse.angle * M_PI / 180.;
    double x0 = ellipse.center.x;
    double y0 = ellipse.center.y;
    double A = (cos(alfa) * cos(alfa))/(a*a)+(sin(alfa) * sin(alfa)) / (b*b);
    double B = -2 * cos(alfa) * sin(alfa) * (1 / (a*a) - 1 / (b*b) );
    double C = (sin(alfa) * sin(alfa))/(a*a)+(cos(alfa) * cos(alfa)) / (b*b);
    double D = -1 * (2 * A * x0 + y0 * B);
    double E = -1 * (2 * C * y0 + B * x0);
    double F = A * x0 * x0 + C * y0 * y0 + B * x0 * y0 - 1;
    cv::Mat conic = (cv::Mat_<double>(3, 3) <<
                     A , B/2 , D/2,
                     B/2 , C , E/2,
                     D/2 , E/2 , F);
    return conic;
}

cv::RotatedRect Conic2Ellipse(cv::Mat Conic)
{
    // Conic2Ellipse: This function converts the ellipse from the matrix representation.
    // to the rotated rectangle representation
    // input: a 3 by 3 matrix
    // output: a rotated rectangle.
    double A = Conic.at<double>(0,0);
    double B = Conic.at<double>(0,1);
    double C = Conic.at<double>(1,1);
    double D = Conic.at<double>(0,2);
    double E = Conic.at<double>(1,2);
    double F = Conic.at<double>(2,2);

    cv::Mat C22 = (cv::Mat_<double> (2,2) <<
                   A , B ,
                   B , C );

    double x0 = (C * D - B * E) / (B * B - A * C);
    double y0 = (A * E - B * D) / (B * B - A * C);

    double nom = 2 * ( A * E * E + C * D * D + F * B * B - 2 * B * D * E - A * C * F);
    double part1 = std::sqrt((A - C) * (A - C) + 4 * B * B);

    double denom1 = (B * B - A * C) * (part1 - (A + C));
    double denom2 = (B * B - A * C) * (-part1 - (A + C));

    cv::RotatedRect ellipse;
    ellipse.center.x = x0;
    ellipse.center.y = y0;

    ellipse.size.height = 2 * std::sqrt( nom / denom1 );
    ellipse.size.width = 2 * std::sqrt ( nom / denom2 );
    ellipse.angle = 0;//atan(b / (a - c) )/2;

    return ellipse;
}

cv::Mat MarkerDetection::Filter(cv::Mat binary_image)
{
    // Filter: applies median filter to the binary image
    cv::medianBlur(binary_image,binary_image,11);
    return binary_image;
}

std::vector<cv::RotatedRect> MarkerDetection::EillipseFitting(cv::Mat binary_image ,
    std::vector<double>& error_vec)
{
    // EillipseFitting: In this function, an ellipse is fitted to every cluster
    // of pixels in a binary image.
    // input: A binary image
    // Output: a vector of fitted ellipses; an ellipse is represented by a rotated rectangle
    cv::Mat temp_img = binary_image.clone();
    cv::cvtColor(temp_img,temp_img,cv::COLOR_GRAY2BGR);
    std::vector<cv::RotatedRect> marks;
    //std::vector<double> error_vec;
    int num_of_ellipses = 0;
    // find contours of objects
    std::vector<std::vector<cv::Point> > contours;
    cv::findContours(binary_image , contours ,
                     cv::RETR_EXTERNAL , cv::CHAIN_APPROX_NONE);
    for (int i = 0 ; i < contours.size() ; i++)
    {
        std::vector<cv::Point> contour = contours[i];
        // if the circumstance of a circle is less than 8 pixels, it is removed.
        if (contour.size()<8)
            continue;
        // fit an ellipse to the contours
        cv::RotatedRect box = fitEllipse(contour);
        //
        if (box.size.area() < 4) // 2 by 2 pixels
            continue;
        // change rotated box to an ellipse equation
        double sum_error = 0.;
        double a = 0.5 * box.size.width;
        double b = 0.5 * box.size.height;
        if (a==0||b==0)
            continue;
        double alfa = box.angle * M_PI / 180.;
        double x0 = box.center.x;
        double y0 = box.center.y;
        double c1 = (cos(alfa) * cos(alfa))/(a*a)+(sin(alfa) * sin (alfa)) / (b*b);
        double c2 = -2. * cos(alfa) * sin(alfa) * (1. / (a*a) - 1. / (b*b) );
        double c3 = (sin(alfa) * sin(alfa))/(a*a)+(cos(alfa) * cos (alfa)) / (b*b);
        // estimating out of ellipse geometric error
        for (int j = 0 ; j < contour.size() ; j++)
        {
            cv::Point p = contour[j];
            double pixel_error = c1 * (p.x - x0) * (p.x - x0) + c2 * (p.x - x0) * (p.y - y0) + c3 * (p.y - y0) * (p.y - y0) - 1;
            sum_error = sum_error + pixel_error * pixel_error;
        }
        double error = std::sqrt(sum_error) / std::sqrt(box.size.area());
        if (error < out_of_ellipse_thr)
        {
            marks.push_back(box);
            num_of_ellipses++;
//            std::cout << " object: " << num_of_ellipses << " out_of_ellipse error: " << error
//                      << " ;area of ellipse is " << box.size.area() << std::endl;
            cv::ellipse(temp_img,box,cv::Scalar(0,0,255));
            error_vec.push_back(error);
        }
    }
    std::vector<double> sorted_error =  error_vec;
    std::sort(sorted_error.begin(), sorted_error.end());
//    std::cout << sorted_error[max_of_ellipses-1] << "::;" << error_vec.size() << std::endl;
    for (int i = error_vec.size() -1  ; i > -1 ; i--)
        if ( (error_vec[i] > sorted_error[max_of_ellipses-1]) && (max_of_ellipses < error_vec.size()) )
        {
            //std::cout << " i: " << i << " error: " << error_vec[i] << std::endl;
            marks.erase(marks.begin()+i);
            error_vec.erase(error_vec.begin()+i);
        }
    return marks;
}

cv::Mat MarkerDetection::ColorMask(cv::Mat image)
{
    // ColorMask: This function masks every color except red (with different illuminations)
    // input: A color image
    // Output: A binary image
    cv::Mat hls_image = image.clone();
    cv::cvtColor(hls_image, hls_image, cv::COLOR_BGR2HLS);
    // Threshold the HSL image, keep only the red pixels
    cv::Mat lower_red_hue_range; // 0-10
    cv::Mat upper_red_hue_range; // 170-179
    cv::inRange(hls_image, cv::Scalar(0, 10, 50), cv::Scalar(lower_threshold, 255, 255), lower_red_hue_range);
    cv::inRange(hls_image, cv::Scalar(upper_threshold, 10, 50), cv::Scalar(180, 255, 255), upper_red_hue_range);
    // Combine the above two images
    cv::Mat red_hue_image;
    cv::addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0, red_hue_image);
    return red_hue_image;
}

cv::Mat PoseFromAConic(cv::RotatedRect ellipse, cv::Mat K , double r)
{
    // This function computes the pose of an ellipse in the image space
    // knowning it is a circle with radius r is real world.
    // input: detected ellipse in the image space
    // input: camera calibration matrix, K
    // input: radius of the marks in real world
    // output: 3*1 marix stating location of the mark with respect to the camera
    std::vector<std::vector<cv::Mat> > Rts;
    cv::Mat Q = (cv::Mat_<double> (3,3) <<
                 1/(r*r) , 0 , 0 ,
                 0 , 1/(r*r) , 0 ,
                 0, 0 , -1);
//    if (ellipse.size.width == 0|| ellipse.size.height == 0)
//        return cv::Mat;
    cv::Mat B = Ellipse2Conic(ellipse);
    cv::Mat A = K.t() * B * K;
    cv::Mat A1;
    A.convertTo(A1 , CV_64FC1);
    cv::Mat eigenValue(1,3,CV_64F);
    cv::Mat eigenVector(3,3,CV_64F);
    cv::eigen(A1 , eigenValue , eigenVector);
    double l1 = eigenValue.at<double>(0,0);
    double l2 = eigenValue.at<double>(0,1);
    double l3 = eigenValue.at<double>(0,2);
    double k1 = l1 * r * r;
    double k2 = l2 * r * r;
    double c3_squared1 = -1 * (l1 * l1 * l1 * r * r) / (l1 * l2 * l3);
    double c3_squared2 = -1 * (l2 * l2 * l2 * r * r) / (l1 * l2 * l3);
    double d_squared1 = 2 * r * r - (l1 * (l1 * l2 + l1 * l3 + l2 * l3) * r * r) / (l1 * l2 * l3);
    double d_squared2 = 2 * r * r - (l2 * (l1 * l2 + l1 * l3 + l2 * l3) * r * r) / (l1 * l2 * l3);
    double c2_squared1 = d_squared1 - c3_squared1;
    double c2_squared2 = d_squared2 - c3_squared2;
    cv::Mat W, Vt , D;
    cv::SVD::compute(A1 ,D, W, Vt);

    /*///////////////////////////////////////////////////////////////*/
    cv::Mat C1 = (cv::Mat_<double> (3,3) <<
                  1 , 0 , 0 ,
                  0 , 1 , std::sqrt(c2_squared1) ,
                  0 , 0 , std::sqrt(c3_squared1));
    cv::Mat B1 = k1 * C1.t().inv() * Q * C1.inv();
    cv::Mat W1, Vt1 , D1;
    cv::SVD::compute(B1 , D1 , W1 , Vt1);
    cv::Mat R1 = W * Vt1;
    cv::Mat c1 = (cv::Mat_<double> (3,1) <<
                  0  , std::sqrt(c2_squared1) , std::sqrt(c3_squared1));
    cv::Mat t1 = -R1 * c1;

    /*///////////////////////////////////////////////////////////////*/
    cv::Mat C2 = (cv::Mat_<double> (3,3) <<
                  1 , 0 , 0 ,
                  0 , 1 , std::sqrt(c2_squared2) ,
                  0 , 0 , std::sqrt(c3_squared2));
    cv::Mat B2 = k2 * C2.t().inv() * Q * C2.inv();
    cv::Mat W2, Vt2 , D2;
    cv::SVD::compute(B2 , D2 , W2 , Vt2);
    cv::Mat R2 = W * Vt2;
    cv::Mat c2 = (cv::Mat_<double> (3,1) <<
                  0  , std::sqrt(c2_squared2) , std::sqrt(c3_squared2));
    cv::Mat t2 = -R2 * c2;

    /*///////////////////////////////////////////////////////////////*/
    cv::Mat C3 = (cv::Mat_<double> (3,3) <<
                  1 , 0 , 0 ,
                  0 , 1 , -std::sqrt(c2_squared1) ,
                  0 , 0 , std::sqrt(c3_squared1));
    cv::Mat B3 = k1 * C3.t().inv() * Q * C3.inv();
    cv::Mat W3, Vt3 , D3;
    cv::SVD::compute(B3 , D3 , W3 , Vt3);
    cv::Mat R3 = W * Vt3;
    cv::Mat c3 = (cv::Mat_<double> (3,1) <<
                  0  , -std::sqrt(c2_squared1) , std::sqrt(c3_squared1));
    cv::Mat t3 = -R3 * c3;

    /*///////////////////////////////////////////////////////////////*/
    cv::Mat C4 = (cv::Mat_<double> (3,3) <<
                  1 , 0 , 0 ,
                  0 , 1 , -std::sqrt(c2_squared2) ,
                  0 , 0 , std::sqrt(c3_squared2));
    cv::Mat B4 = k2 * C4.t().inv() * Q * C4.inv();
    cv::Mat W4, Vt4 , D4;
    cv::SVD::compute(B4 , D4 , W4 , Vt4);
    cv::Mat R4 = W * Vt4;
    cv::Mat c4 = (cv::Mat_<double> (3,1) <<
                  0  , -std::sqrt(c2_squared2) , std::sqrt(c3_squared2));
    cv::Mat t4 = -R4 * c4;
//    std::cout << "t1 = " << t1 << std::endl;
//    std::cout << "t2 = " << t2 << std::endl;
//    std::cout << "t3 = " << t3 << std::endl;
//    std::cout << "t4 = " << t4 << std::endl;
    cv::Mat t;
    if (t2.at<double>(2,0) > 0)
        return t2;
    else  if (t4.at<double>(2,0) > 0)
        return t4;
}

/* Combinator: This function creates a vecotr of all possible sequences.
 * For instance, lets assumed that 10 ellipses have been found and the marker
 * has four ellipses (circles). This funciton gives a vector of possible answer
 * such as {0,0,0,1,0,0,1,1,0,1}
 * Input: The number of observed ellipses,
 *        The number of ellipses (circles) in the marker.
 * Output: A vector of possible answers
 */
static std::vector<std::vector<bool> > Combinator
(unsigned int numOfObserved , unsigned int numOfMarkers)
{
    /*set_size of power set of a set with set_size
      n is (2**n -1)*/
    unsigned int pow_set_size = pow(2, numOfObserved);
    std::vector<std::vector<bool> > solutions;
    /*Run from counter 000..0 to 111..1*/
    for(int counter = 0; counter < pow_set_size; counter++)
    {
        std::vector<bool> isSolution;
        unsigned int num = 0;
        for(int j = 0; j < numOfObserved; j++)
        {
            /* Check if jth bit in the counter is set
             If set then pront jth element from set */
            if(counter & (1<<j))
            {
                isSolution.push_back(true);
                num++;
            }
            else
                isSolution.push_back(false);
        }
        if (num == numOfMarkers)
            solutions.push_back(isSolution);
    }
    return solutions;
}

std::vector<cv::Mat> MarkerDetection::EstimatePose
    (std::vector<cv::RotatedRect> Cvec, cv::Mat K , double r)
{
    std::vector<cv::Mat> tMarks;
    for (int i = 0 ; i < Cvec.size() ; i++)
    {
        cv::Mat t = PoseFromAConic(Cvec[i] , K , r);
        tMarks.push_back(t);
    }
    return tMarks;
}

/* MarkerDetection: This detects the marks of the markers
 * input: A vector of marks
 *        calibration matrix K
 *        radius of the circles in the marker space
 * Output: a vector of marks of the marker
 *         (by reference) A vector of the position of marks of the marker
 */
std::vector<cv::RotatedRect> MarkerDetection::MarkerFinder
    (std::vector<cv::RotatedRect> Cvec , cv::Mat K , double r,
     std::vector<cv::Mat> & Ts)
{
    //std::vector<cv::Mat> Cmat = Ellipses2Conics(Cvec);
    //cv::Mat img(1200,1600,CV_8UC3,cv::Scalar(255,255,255));

    std::vector<cv::Mat> tMarks;
    std::vector<cv::RotatedRect> Cvec_mod;
    for (int i = 0 ; i < Cvec.size() ; i++)
    {
        cv::Mat t = PoseFromAConic(Cvec[i] , K , r);
        double d = cv::norm(t);
        //std::cout << "t = " << t << std::endl;
        if ( (d > 100.) || (t.at<double>(2,0) < 0))
            continue;
        else
        {
            Cvec_mod.push_back(Cvec[i]);
            tMarks.push_back(t);
        }
    }
    std::vector<std::vector<bool> > combinations = Combinator(tMarks.size() , n_marks);
    std::vector<bool> best_combination;
    double n = 1. / boost::lexical_cast<double>(n_marks);
    double min_dist = INFINITY;
    for (int i = 0 ; i < combinations.size() ; i++)
    {
        std::vector<bool> sol = combinations[i];
        cv::Mat sum(3,1,CV_64F,cv::Scalar(0));
        double sum_x = 0, sum_y = 0, sum_z = 0;
        for (int j = 0 ; j < sol.size(); j++ )
            if (sol[j])
            {
                cv::Mat t = tMarks[j];
                double t_x = tMarks[j].at<double>(0,0);
                double t_y = tMarks[j].at<double>(1,0);
                double t_z = tMarks[j].at<double>(2,0);
                sum_x = sum_x + n * t_x;
                sum_y = sum_y + n * t_y;
                sum_z = sum_z + n * t_z;
                sum = sum + n * t;
            }
        double max_xy = 0 , max_xyz = 0;
        for (int j = 0 ; j < sol.size(); j++ )
        {
            if (sol[j])
            {
                cv::Mat t = tMarks[j];
                double t_x = tMarks[j].at<double>(0,0);
                double t_y = tMarks[j].at<double>(1,0);
                double t_z = tMarks[j].at<double>(2,0);
                double dist_x = (sum_x - t_x) ;
                double dist_y = (sum_y - t_y) ;
                double dist_z = (sum_z - t_z) ;
                double dist_xy = std::sqrt(dist_x * dist_x + dist_y * dist_y);
                double dist_xyz = std::sqrt(dist_x * dist_x + dist_y * dist_y + dist_z * dist_z);
                if (max_xy < dist_xy)
                    max_xy = dist_xy;
                if (max_xyz < dist_xyz)
                    max_xyz = dist_xyz;
            }
        }
        if ((max_xyz < min_dist) && (max_xy < 0.5 /*50cm*/))
        {
            best_combination =  sol;
            min_dist = max_xyz;
        }
    }

     std::vector<cv::RotatedRect> marks;
     //std::vector<cv::Mat> Ts;
     for (int i = 0 ; i < best_combination.size() ; i++)
        if (best_combination[i])
        {
            marks.push_back(Cvec_mod[i]);
            Ts.push_back(tMarks[i]);
        }
    return marks;
}
