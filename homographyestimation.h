#ifndef HOMOGRAPHYESTIMATION_H
#define HOMOGRAPHYESTIMATION_H
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <boost/lexical_cast.hpp>
#include <algorithm>
#include <math.h>

class HomographyEstimation
{
public:
    HomographyEstimation();
    cv::Mat HomographyFinder
        (std::vector<cv::RotatedRect> marks , std::vector<cv::RotatedRect> marker);

};

#endif // HOMOGRAPHYESTIMATION_H
