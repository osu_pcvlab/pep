#-------------------------------------------------
#
# Project created by QtCreator 2016-09-05T14:07:06
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = PEP_v16
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

#OpenCV
INCLUDEPATH += /usr/local/include
LIBS += /usr/local/lib/libopencv_core.so
LIBS += /usr/local/lib/libopencv_highgui.so
LIBS += /usr/local/lib/libopencv_imgproc.so
LIBS += /usr/local/lib/libopencv_imgcodecs.so
LIBS += /usr/local/lib/libopencv_calib3d.so
LIBS += /usr/local/lib/libopencv_features2d.so
LIBS += /usr/local/lib/libopencv_highgui.so
LIBS += /usr/local/lib/libopencv_video.so
LIBS += /usr/local/lib/libopencv_videoio.so

SOURCES += main.cpp \
    markerdetection.cpp \
    markerdesign.cpp \
    homographyestimation.cpp \
    markerevaluation.cpp

HEADERS += \
    markerdetection.h \
    markerdesign.h \
    homographyestimation.h \
    markerevaluation.h
