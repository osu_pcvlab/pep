#include "homographyestimation.h"

HomographyEstimation::HomographyEstimation()
{
}

/* OrderEllipses: This function sorts the ellipses clockwise. This function
 * prevents conic mismatches later on.
 * Input: The (unorganized) observed ellipses,
 * Output: The ordered observed ellipses.
 */
std::vector<cv::RotatedRect> OrderEllipses
(std::vector<cv::RotatedRect> unorderedEllipses)
{
    std::vector<cv::RotatedRect> orderedEllipses;
    double x_origin = 0, y_origin = 0;
    int n = unorderedEllipses.size();
    double coeff = 1. / (double)(n);
    for (int i = 0 ; i < n ; i++)
    {
        x_origin = x_origin + coeff * unorderedEllipses[i].center.x;
        y_origin = y_origin + coeff * unorderedEllipses[i].center.y;
    }
    // finding azimuth zero
    double min_dxy = INFINITY;
    double angle_ref;
    for (int i = 0 ; i < n ; i++)
    {
        double dx = unorderedEllipses[i].center.x - x_origin;
        double dy = unorderedEllipses[i].center.y - y_origin;
        if (min_dxy > dx + dy)
        {
            min_dxy = dx + dy;
            angle_ref  = atan2(dy , dx );
        }
    }
    // calculating the angles of ellipses
    std::vector<double> angle_vector;
    for (int i = 0 ; i < n ; i++)
    {
        double dx = unorderedEllipses[i].center.x - x_origin;
        double dy = unorderedEllipses[i].center.y - y_origin;
        double angle = atan2(dy , dx ) - angle_ref;
        if (angle < 0)
            angle = angle + 2 * M_PI;
        if (angle > 2 * M_PI)
            angle = angle - 2 * M_PI;
        angle_vector.push_back(angle);
    }
    // order the ellipses
    std::vector<double> sorted_angle = angle_vector;
    std::sort( sorted_angle.begin(),sorted_angle.end() );
    for (int i = 0 ; i < sorted_angle.size() ; i++)
        for (int j = 0 ; j < angle_vector.size() ; j++)
            if (angle_vector[j] == sorted_angle[i])
            {
                orderedEllipses.push_back(unorderedEllipses[j]);
                continue;
            }
    return orderedEllipses;
}

/* Ellipses2Conics: This function converts the ellipses from rotated rectangle representation
 * to the matrix representation.
 * Input: a vector ellipses represented by rotated rectangle,
 * Output: a vector ellipses represented by 3 by 3 matrix.
 */
static std::vector<cv::Mat> Ellipses2Conics
(std::vector<cv::RotatedRect> ellipses)
{
    std::vector<cv::Mat> conics;
    for (int i = 0 ; i < ellipses.size(); i++ )
    {
        cv::RotatedRect box = ellipses[i];
        double a = 0.5 * box.size.width;
        double b = 0.5 * box.size.height;
        if (a==0||b==0)
            continue;
        double alfa = box.angle * M_PI / 180.;
        double x0 = box.center.x;
        double y0 = box.center.y;
        double A = (cos(alfa) * cos(alfa))/(a*a)+(sin(alfa) * sin(alfa)) / (b*b);
        double B = -2 * cos(alfa) * sin(alfa) * (1 / (a*a) - 1 / (b*b) );
        double C = (sin(alfa) * sin(alfa))/(a*a)+(cos(alfa) * cos(alfa)) / (b*b);
        double D = -1 * (2 * A * x0 + y0 * B);
        double E = -1 * (2 * C * y0 + B * x0);
        double F = A * x0 * x0 + C * y0 * y0 + B * x0 * y0 - 1;
        cv::Mat conic = (cv::Mat_<double>(3, 3) <<
                         A , B/2 , D/2,
                         B/2 , C , E/2,
                         D/2 , E/2 , F);
        conics.push_back(conic);
    }
    return conics;
}

/* NormalizeEllipses: This function normalizes the observed ellipses.
 * It means that the origin of the coordinate system is transferred to
 * the center of mass of ellipses and the distance of farthest ellipse
 * from the origin is scaled to square root of 2.
 * Input: The ordered observed ellipses,
 * Output: The normalized observed ellipses.
 */
std::vector<cv::RotatedRect> NormalizeEllipses
(std::vector<cv::RotatedRect> orderedEllipses, cv::Mat& T)
{
    // change the origin
    double x_origin = 0, y_origin = 0;
    int n = orderedEllipses.size();
    double coeff = 1. / (double)(n);
    std::vector<cv::RotatedRect> normalizedEllipses;
    for (int i = 0 ; i < n ; i++)
    {
        x_origin = x_origin + coeff * orderedEllipses[i].center.x;
        y_origin = y_origin + coeff * orderedEllipses[i].center.y;
    }
    double d_max = 0;
    for (int i = 0 ; i < n ; i++)
    {
        double dx = orderedEllipses[i].center.x - x_origin;
        double dy = orderedEllipses[i].center.y - y_origin;
        if (d_max < std::sqrt(dx*dx+dy*dy) )
            d_max = std::sqrt(dx*dx+dy*dy);
    }
    // scaling the observed ellipses
    double scale = std::sqrt(2) / d_max;
    T = (cv::Mat_<double>(3, 3) <<
                     scale , 0 , -scale*x_origin,
                    0 , scale , -scale*y_origin,
                     0 , 0 , 1);

    for (int i = 0 ; i < n ; i++)
    {
        cv::RotatedRect r;
        r.size.width = scale * orderedEllipses[i].size.width;
        r.size.height = scale * orderedEllipses[i].size.height;
        r.center.x = scale * (orderedEllipses[i].center.x - x_origin);
        r.center.y = scale * ( orderedEllipses[i].center.y - y_origin);
        r.angle = orderedEllipses[i].angle;
        normalizedEllipses.push_back(r);
    }
    return normalizedEllipses;
}

/* CalcHomography: This function calcuates the homography matrix between the ellipses
 * in the marker and image spaces. The out of homography error is calculated and passed
 * by pointer.
 * Input: The ellipses of the marker in the marker space
 *        The ellipses of the marker in the image space
 *        The out of homography error; This value is actually estimated in the function
 *          and is passed by pointer
 * Output:  3 by 3 homography matrix between ellipses in the marker and image spaces.
 */
cv::Mat CalcHomography(std::vector<cv::Mat> marker ,
                       std::vector<cv::Mat> obs_conic)
{
    std::vector<cv::Mat> marker_conic;
    for (int i = 0 ; i < marker.size(); i++ )
    {
        // normalization of scale
        double scale = cbrt(cv::determinant(obs_conic[i]) / cv::determinant(marker[i]));
        marker_conic.push_back(marker[i] * scale);
    }
    // Calculating M (9 by 9) matrix
    cv::Mat M(0,0,CV_64F,cv::Scalar(0));
    for (int i = 0 ; i < marker_conic.size(); i++)
        for (int j = 0 ; j < obs_conic.size(); j++)
        {
            if (i!=j)
            {
                cv::Mat C_marker = marker_conic[i].inv() * marker_conic[j];
                cv::Mat C_image = obs_conic[i].inv() * obs_conic[j];
                cv::Mat m(9,9,CV_64F,cv::Scalar(0));
                cv::Mat m11(3,3,CV_64F,cv::Scalar(0));
                cv::Mat m12(3,3,CV_64F,cv::Scalar(0));
                cv::Mat m13(3,3,CV_64F,cv::Scalar(0));
                cv::Mat m21(3,3,CV_64F,cv::Scalar(0));
                cv::Mat m22(3,3,CV_64F,cv::Scalar(0));
                cv::Mat m23(3,3,CV_64F,cv::Scalar(0));
                cv::Mat m31(3,3,CV_64F,cv::Scalar(0));
                cv::Mat m32(3,3,CV_64F,cv::Scalar(0));
                cv::Mat m33(3,3,CV_64F,cv::Scalar(0));
                m11 = C_image.at<double>(0,0) * cv::Mat::eye(3,3,CV_64F) - C_marker.t();
                m12 = C_image.at<double>(0,1) * cv::Mat::eye(3,3,CV_64F);
                m13 = C_image.at<double>(0,2) * cv::Mat::eye(3,3,CV_64F);
                m21 = C_image.at<double>(1,0) * cv::Mat::eye(3,3,CV_64F);
                m22 = C_image.at<double>(1,1) * cv::Mat::eye(3,3,CV_64F) - C_marker.t();
                m23 = C_image.at<double>(1,2) * cv::Mat::eye(3,3,CV_64F);
                m31 = C_image.at<double>(2,0) * cv::Mat::eye(3,3,CV_64F);
                m32 = C_image.at<double>(2,1) * cv::Mat::eye(3,3,CV_64F);
                m33 = C_image.at<double>(2,2) * cv::Mat::eye(3,3,CV_64F) - C_marker.t();
                cv::Mat m1,m2,m3;
                cv::hconcat(m11,m12,m1);
                cv::hconcat(m1,m13,m1);
                cv::hconcat(m21,m22,m2);
                cv::hconcat(m2,m23,m2);
                cv::hconcat(m31,m32,m3);
                cv::hconcat(m3,m33,m3);
                cv::vconcat(m1,m2,m);
                cv::vconcat(m,m3,m);
                if (M.size().area() == 0)
                    M = m;
                else
                    cv::vconcat(M,m,M);
            }
        }
    // estimate h vector
    cv::Mat w, u, vt;
    cv::SVD::compute(M, w, u, vt);
    cv::Mat v = vt.t();
    cv::Mat h = v.col(v.cols - 1);
    // calculating out of homography error
    cv::Mat error_vec = M * h;
    cv::Mat sse_mat = error_vec.t() * error_vec;
    double sse = sse_mat.at<double>(0,0);
    double error = std::sqrt(sse) ;// (n_marks * (n_marks-1)); // per pair of ellipses
   // std::cout << " M = " << M << std::endl;
    std::cout << " error = " << error << std::endl;
    return h;
}

cv::Mat HomographyEstimation::HomographyFinder
(std::vector<cv::RotatedRect> obsMarks , std::vector<cv::RotatedRect> marker)
{
    // This fucntion estimates the homography transformation based on the marker
    // in the marker and image spaces
    // input: maker in the image space
    //        marker in the marker space
    // output: The homography transformation matrix
    std::vector<cv::RotatedRect> ordered_obsMarks = OrderEllipses
            (obsMarks);
    cv::Mat T_marker;
    std::vector<cv::RotatedRect> normalized_marker = NormalizeEllipses(marker, T_marker);
    cv::Mat T_obsMarks;
    std::vector<cv::RotatedRect> normalized_obsMarks = NormalizeEllipses(ordered_obsMarks, T_obsMarks);
    std::vector<cv::Mat> conics_marker = Ellipses2Conics(normalized_marker);
    std::vector<cv::Mat> conics_obsMarks = Ellipses2Conics(normalized_obsMarks);
    cv::Mat h_vec = CalcHomography(conics_marker ,conics_obsMarks );
    cv::Mat H_tilde(3,3,CV_64F,cv::Scalar(0));
    H_tilde.at<double>(0,0) = h_vec.at<double>(0,0);
    H_tilde.at<double>(0,1) = h_vec.at<double>(1,0);
    H_tilde.at<double>(0,2) = h_vec.at<double>(2,0);
    H_tilde.at<double>(1,0) = h_vec.at<double>(3,0);
    H_tilde.at<double>(1,1) = h_vec.at<double>(4,0);
    H_tilde.at<double>(1,2) = h_vec.at<double>(5,0);
    H_tilde.at<double>(2,0) = h_vec.at<double>(6,0);
    H_tilde.at<double>(2,1) = h_vec.at<double>(7,0);
    H_tilde.at<double>(2,2) = h_vec.at<double>(8,0);
    cv::Mat H = T_obsMarks.inv() * H_tilde * T_marker;
    return H;
}
