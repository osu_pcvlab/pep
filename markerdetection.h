#ifndef MARKERDETECTION_H
#define MARKERDETECTION_H
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <boost/lexical_cast.hpp>
#include <algorithm>
#include <math.h>
class MarkerDetection
{
public:
    MarkerDetection();
    cv::Mat ColorMask(cv::Mat image);
    cv::Mat Filter(cv::Mat binary_image);
    std::vector<cv::RotatedRect> EillipseFitting(cv::Mat binary_image,
               std::vector<double>& error_vec);
    std::vector<cv::Mat> EstimatePose
        (std::vector<cv::RotatedRect> Cvec, cv::Mat K , double r);
    std::vector<cv::RotatedRect> MarkerFinder
        (std::vector<cv::RotatedRect> Cvec , cv::Mat K , double r,
         std::vector<cv::Mat> & Ts);
    std::vector<cv::RotatedRect> Back_Fore_projection
        (std::vector<cv::RotatedRect> marker , cv::Mat H);
};

#endif // MARKERDETECTION_H
